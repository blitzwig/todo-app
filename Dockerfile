FROM node:slim
LABEL maintainer = "richard.chris@yahoo.com"
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./app/ ./
RUN npm install
RUN npm audit fix
CMD ["node", "app.js"]
